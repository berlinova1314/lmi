﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        #region variables
        Random x = new Random();
        Random y = new Random();
        public Point p = new Point();
        public Point[] pi = new Point[2];
        int Status_exp = 0;
        DateTime Start;
        DateTime Stoped;
        TimeSpan Elapsed = new TimeSpan();
        public Point s;
        #endregion

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            lbRez.Items.Clear();
            lbRez.Items.Add("Результати");

            expir();
        }

        private Point DrowObject(Point p)
        {
            var el = new Rectangle
            {
                Width = 12,
                Height = 12
            };

            p.X = x.Next(Convert.ToInt32(this.Width - el.Width));
            p.Y = y.Next(Convert.ToInt32(icDrow.ActualHeight - el.Height));

            el.Fill = Brushes.Yellow;

            InkCanvas.SetLeft(el, p.X);
            InkCanvas.SetTop(el, p.Y);

            icDrow.Children.Add(el);
            return p;
        }

        public void expir()
        {
            Array.Clear(pi, 0, pi.Length - 1);

            Start = new DateTime(0);
            icDrow.Children.Clear();
            icDrow.Strokes.Clear();

            for (int i = 0; i < 2; i++)
            {
                pi[i] = DrowObject(p);
            }

            Status_exp = Status_exp + 1;
        }

        private void icUp(object sender, MouseButtonEventArgs e)
        {
            if (Status_exp > 0)
            {
                var n = Array.FindAll(pi, element => (Math.Abs(element.X - e.GetPosition(this).X) < 20) && (Math.Abs(element.Y - e.GetPosition(this).Y) < 20));

                if (Array.Exists(pi, element => (Math.Abs(element.X - e.GetPosition(this).X) < 20) && (Math.Abs(element.Y - e.GetPosition(this).Y) < 20)))
                {
                    if (!((Math.Abs(s.X - e.GetPosition(this).X) < 10) && (Math.Abs(s.Y - e.GetPosition(this).Y) < 10)))
                    {
                        s = e.GetPosition(this);

                        if (Start.Ticks == 0)
                        {
                            Start = DateTime.Now;
                        }
                        else
                        {
                            Stoped = DateTime.Now;
                            Elapsed = Stoped.Subtract(Start);

                            var elapsedTicks = Stoped.Ticks - Start.Ticks;
                            var elapsedSpan = new TimeSpan(elapsedTicks);
                            var rez = Math.Sqrt(Math.Pow(pi[0].X - pi[1].X, 2) + Math.Pow(pi[0].Y - pi[1].Y, 2));

                            lbRez.Items.Add("\nЕксперимент №" + Status_exp.ToString() + "\nЧас: " + elapsedSpan.Milliseconds.ToString() + "\nВідстань: " + rez);

                            if (Status_exp < 100)
                            {
                                expir();
                            }
                        }
                    }
                }
            }
        }

    }
}